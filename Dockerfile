FROM python:3.7-slim-buster


COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR /usr/src/app
COPY developer_test/ .

RUN python manage.py collectstatic --no-input && \
    find /usr/src/app -type d -exec chmod o+rx {} \; && \
    find /usr/src/app -type f -exec chmod o+r {} \;

USER nobody:nogroup

CMD ["gunicorn", "--workers=2", "--bind=0.0.0.0:8000", "--env", "PYTHONUNBUFFERED=1", "developer_test.wsgi"]
