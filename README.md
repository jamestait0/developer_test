# Developer Test

This is a simple little project to demonstrate Django, Django-REST-Framework,
and Backbone.js, and using Bootstrap CSS. It has a simple web UI for listing
and creating foreign exchange trades, and a RESTful API. It interfaces by
default with the service at fixer.io to get rates and perform conversions --
you will need an account there and an API key in order for this application to
work.  Moreover, the free API key with Fixer does not provide access to many of
the API endpoints, and does not allow specifying the base currency for
conversions -- you will need a paid API key to get this functionality.

## Quick Start

Create yourself a python virtual environment:

    $ apt install python3-venv
    $ python3 -m venv .venv
    $ . .venv/bin/activate

Install requirements:

    (.venv) $ pip install -r requirements.txt

Create secrets:

    (.venv) $ cat .env
    SECRET_KEY=1234567890abcdef          # Django secret key
    FIXER_API_KEY=1234567890abc          # Fixer API key
    FIXER_API_BASE=http://data.fixer.io  # Fixer API Base URL
    DEBUG=True                           # Django debug - should be off for production!

Run:

    (.venv) $ ./manage.py migrate
    Operations to perform:
      Apply all migrations: admin, auth, contenttypes, forex, sessions
    Running migrations:
      ...
    (.venv) $ ./manage.py runserver
    Watching for file changes with StatReloader
    Performing system checks...

    System check identified no issues (0 silenced).
    June 18, 2019 - 10:36:28
    Django version 2.2, using settings 'developer_test.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

Now you can navigate to http://localhost:8000/ in your browser and see a list
of existing trades, if any, and create new trades. The UI is self-explanatory.
