import random
import string

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from api import fixer_api


def id_generator():
    # Based on https://stackoverflow.com/a/37359808
    chars = string.ascii_letters + string.digits
    _id = 'TR' + ''.join(random.choice(chars) for _ in range(7))
    while Trade.objects.filter(pk=_id).exists():
        _id = 'TR' + ''.join(random.choice(chars) for _ in range(7))
    return _id


def validate_symbol(value):
    fixer = fixer_api.Client(settings.FIXER_API_KEY)
    valid_symbols = fixer.symbols()
    if value.upper() not in valid_symbols:
        raise ValidationError(
            _('%(value)s is not a valid symbol'),
            params={'value': value},
        )


class Trade(models.Model):
    """Represents a trade."""

    trade_id = models.CharField(
        primary_key=True, max_length=9, default=id_generator,
        validators=[RegexValidator(regex='^TR[a-zA-Z0-9]{7}$')]
    )
    sell_currency = models.CharField(
        max_length=3, validators=[validate_symbol]
    )
    sell_amount = models.DecimalField(decimal_places=2, max_digits=15)
    buy_currency = models.CharField(max_length=3, validators=[validate_symbol])
    buy_amount = models.DecimalField(decimal_places=2, max_digits=15)
    exchange_rate = models.DecimalField(decimal_places=6, max_digits=15)
    date_booked = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        for field in ('buy_currency', 'sell_currency'):
            val = getattr(self, field, False)
            if val:
                setattr(self, field, val.upper())
        fixer = fixer_api.Client(settings.FIXER_API_KEY)
        conversion = fixer.convert(
            self.sell_currency, self.buy_currency, self.sell_amount)
        self.buy_amount = conversion['result']
        self.exchange_rate = conversion['rate']
        return super().save(*args, **kwargs)
