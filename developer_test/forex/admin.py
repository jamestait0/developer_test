from django.contrib import admin

from .models import Trade


class TradeAdmin(admin.ModelAdmin):
    list_display = (
        'trade_id',
        'sell_currency', 'sell_amount',
        'buy_currency', 'buy_amount',
        'exchange_rate',
        'date_booked',
    )


admin.site.register(Trade, TradeAdmin)
