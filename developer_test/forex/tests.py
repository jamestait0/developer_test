from functools import partial
from unittest.mock import patch

from django.test import TestCase

from .models import (
    Trade,
    id_generator,
    validate_symbol,
)
from api.fixer_api import Client
from api.tests import fixer_factory


class IDGeneratorTestCase(TestCase):
    def test_id_generator(self):
        _id = id_generator()
        self.assertRegex(_id, r'^TR[a-zA-Z0-9]{7}$')


class ValidateSymbolTestCase(TestCase):
    @patch('api.fixer_api.requests.get')
    def test_symbol_is_valid(self, mock_get):
        output = mock_get.return_value
        output.ok = True
        output.json.return_value = fixer_factory.symbols_response(
            GBP='British Pound Sterling',
        )
        validate_symbol('GBP')
        validate_symbol('gBp')


class FakeFixer(Client):
    FAKE_RATE = 1.652542

    endpoints = {
        'convert': partial(fixer_factory.convert_response, rate=FAKE_RATE),
        'latest': partial(fixer_factory.latest_response, GBP=FAKE_RATE),
        'symbols': partial(
            fixer_factory.symbols_response, GBP='British Pound Sterling'
        ),
    }

    def _get(self, endpoint, **kwargs):
        return self.endpoints[endpoint](**kwargs)


class TradeTestCase(TestCase):
    @patch('api.fixer_api.Client', FakeFixer)
    def test_model_creation(self):
        trade = Trade(sell_currency='EUR', buy_currency='GBP', sell_amount=100)
        trade.save()
        self.assertEqual(100 * FakeFixer.FAKE_RATE, trade.buy_amount)
        self.assertEqual(FakeFixer.FAKE_RATE, trade.exchange_rate)
