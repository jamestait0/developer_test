$(function() {
    // Models
    Symbol = Backbone.Model.extend({});

    Symbols = Backbone.Collection.extend({
        model: Symbol,
        url: '/api/symbols/'
    });

    Trade = Backbone.Model.extend({
        validate: function(attrs, options) {
            if (isNaN(attrs.sell_amount)) {
                return "Sell amount must be numeric";
            }
        }
    });

    Trades = Backbone.Collection.extend({
        model: Trade,
        url: '/api/trades/'
    });

    Conversion = Backbone.Model.extend({
        url: '/api/convert/',

        validate: function(attrs, options) {
            if (isNaN(attrs.sell_amount)) {
                return "Sell amount must be numeric";
            }
        }
    });

    // Views
    SymbolView = Backbone.View.extend({
        tagName: 'option',

        template: _.template($('#symbol-template').html()),

        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    SymbolsView = Backbone.View.extend({
        initialize: function() {
            this.collection = new Symbols();
            this.collection.on('sync', this.render, this);
            this.collection.fetch();
        },

        render: function() {
            _.each(this.collection.models, function(symbol) {
                var view = new SymbolView({model: symbol});
                this.$el.append(view.render().el);
            }, this);
            return this;
        }
    });


    var AppView = Backbone.View.extend({
        el: $('#forexapp'),

        events: {
            "click #new-trade-btn": "showNewTrade",
            "click #book-trade-btn": "bookNewTrade",
            "click #cancel-trade-btn": "hideNewTrade",
            "keyup #sell_amount": "updateConversion",
            "change #sell_currency": "updateConversion",
            "change #buy_currency": "updateConversion"
        },

        initialize: function() {
            this.sell_list = new SymbolsView({el: "#sell_currency"});
            this.buy_list = new SymbolsView({el: "#buy_currency"});
            this.conversion = new Conversion();
            this.conversion.on('invalid', function(model, error) {
                console.log('Invalid');
                $('#errors').html(error);
            });
        },

        showNewTrade: function() {
            $('#trades-list').hide();
            $('#booking-form').show();
        },

        bookNewTrade: function() {
            var sell_currency = $('#sell_currency').children('option:selected').val();
            var buy_currency = $('#buy_currency').children('option:selected').val();
            var sell_amount = $('#sell_amount').val();
            var trade = new Trade({'sell_currency': sell_currency, 'buy_currency': buy_currency, 'sell_amount': sell_amount});
            var trades = new Trades();
            trades.create(trade, {
                success: this.hideNewTrade
            });
        },

        hideNewTrade: function() {
            $('#trades-list').show();
            $('#booking-form').hide();
        },

        updateConversion: _.debounce(function() {
            var sell_currency = $('#sell_currency').children('option:selected').val();
            var buy_currency = $('#buy_currency').children('option:selected').val();
            var sell_amount = $('#sell_amount').val() || '0';
            this.conversion.set({'sell_currency': sell_currency, 'buy_currency': buy_currency, 'sell_amount': sell_amount});
            this.conversion.save(null, {
                success: function(model, response, options) {
                    $('#trade-rate').html(response.rate);
                    $('#buy_amount').val(response.buy_amount);
                }
            });
        }, 1000)
    });

    app = new AppView();
});
