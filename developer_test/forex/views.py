from django.shortcuts import render

from .models import Trade


def index(request):
    trades_list = Trade.objects.order_by('-date_booked')
    context = {
        'trades_list': trades_list,
    }
    return render(request, 'forex/index.html', context)
