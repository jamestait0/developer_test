from rest_framework import serializers

from .models import Trade


class TradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trade
        fields = (
            'trade_id',
            'sell_currency', 'sell_amount',
            'buy_currency', 'buy_amount',
            'exchange_rate',
            'date_booked',
        )
        read_only_fields = (
            'trade_id', 'buy_amount', 'exchange_rate', 'date_booked'
        )


class SymbolSerializer(serializers.Serializer):
    symbol = serializers.CharField(read_only=True, max_length=3)
    description = serializers.CharField(read_only=True)


class ConvertSerializer(serializers.Serializer):
    sell_currency = serializers.CharField(max_length=3)
    buy_currency = serializers.CharField(max_length=3)
    sell_amount = serializers.DecimalField(decimal_places=2, max_digits=15)
    buy_amount = serializers.DecimalField(
        decimal_places=2, max_digits=15, read_only=True)
    rate = serializers.DecimalField(
        decimal_places=6, max_digits=15, read_only=True)
