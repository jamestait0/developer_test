from unittest.mock import patch

from django.conf import settings
from django.test import TestCase

from . import fixer_factory
from ..fixer_api import Client


class FixerAPITestCase(TestCase):
    def setUp(self):
        self.client = Client(settings.FIXER_API_KEY)

    @patch('api.fixer_api.requests.get')
    def test_convert(self, mock_get):
        output = mock_get.return_value
        output.ok = True
        output.json.return_value = fixer_factory.convert_response(
            sell_currency='GBP', buy_currency='JPY',
            amount=25.00, rate=148.972231
        )
        response = self.client.convert('GBP', 'JPY', 25.00)
        self.assertTrue(mock_get.called)
        self.assertDictEqual(response, {
            'from': 'GBP',
            'to': 'JPY',
            'amount': 25,
            'rate': 148.972231,
            'result': 3724.305775,
        })

    @patch('api.fixer_api.requests.get')
    def test_latest(self, mock_get):
        output = mock_get.return_value
        output.ok = True
        output.json.return_value = fixer_factory.latest_response(
            GBP=0.72007, JPY=107.346001
        )
        response = self.client.latest()
        self.assertTrue(mock_get.called)
        self.assertDictEqual(response, {
            'GBP': 0.72007,
            'JPY': 107.346001,
        })

    @patch('api.fixer_api.requests.get')
    def test_symbols(self, mock_get):
        output = mock_get.return_value
        output.ok = True
        output.json.return_value = fixer_factory.symbols_response(
            EUR='Euro',
            GBP='British Pound Sterling',
            JPY='Japanese Yen',
        )
        response = self.client.symbols()
        self.assertTrue(mock_get.called)
        self.assertDictEqual(response, {
            'EUR': 'Euro',
            'GBP': 'British Pound Sterling',
            'JPY': 'Japanese Yen',
        })
