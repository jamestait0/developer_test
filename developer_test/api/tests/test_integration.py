from unittest import skipUnless
from unittest.mock import patch

from decouple import config
from django.conf import settings
from django.test import TestCase

from . import fixer_factory
from ..fixer_api import Client


@skipUnless(config('INCLUDE_INTEGRATION_TESTS', default=False, cast=bool),
            "Set INCLUDE_INTEGRATION_TESTS=True to run integration tests")
class FixerAPIIntegrationTestCase(TestCase):
    def setUp(self):
        self.client = Client(settings.FIXER_API_KEY)

    @skipUnless(config('FIXER_API_BASIC', default=False, cast=bool),
                "Convert requires at least BASIC Fixer subscription")
    def test_convert(self):
        response = self.client.convert('GBP', 'JPY', 25.00)
        self.assertEqual(
            {'from', 'to', 'amount', 'rate', 'result'},
            set(response.keys())
        )
        self.assertEqual('GBP', response['from'])
        self.assertEqual('JPY', response['to'])
        self.assertEqual(25, response['amount'])
        self.assertIsInstance(type(response['rate']), (int, float))
        self.assertIsInstance(type(response['result']), (int, float))

    def test_latest(self):
        response = self.client.latest()
        for (symbol, description) in response.items():
            self.assertTrue(str.isupper(symbol))
            self.assertEqual(3, len(symbol))
            self.assertIsInstance(description, (int, float))

    def test_symbols(self):
        response = self.client.symbols()
        for (symbol, value) in response.items():
            self.assertTrue(str.isupper(symbol))
            self.assertEqual(3, len(symbol))
            self.assertIsInstance(value, str)


@skipUnless(config('INCLUDE_INTEGRATION_TESTS', default=False, cast=bool),
            'Set INCLUDE_INTEGRATION_TESTS=True to run integration tests')
class FixerAPIMockTestCase(TestCase):
    def setUp(self):
        self.client = Client(settings.FIXER_API_KEY)

    def _assert_types_match(self, actual_response, mock_response, key):
        key_type = type(actual_response[key])
        self.assertIsInstance(mock_response[key], key_type)

    def _assert_structures_match(self, actual_response, mock_response):
        actual_keys = list(actual_response.keys())
        for key in actual_keys:
            self._assert_types_match(actual_response, mock_response, key)

    @skipUnless(config('FIXER_API_BASIC', default=False, cast=bool),
                'Convert requires at least BASIC Fixer subscription')
    def test_convert_contract(self):
        actual_response = self.client.convert('GBP', 'JPY', 25.00)

        with patch('api.fixer_api.requests.get') as mock_get:
            output = mock_get.return_value
            output.ok = True
            output.json.return_value = fixer_factory.convert_response(
                sell_currency='GBP', buy_currency='JPY',
                amount=25.00, rate=148.972231
            )
            mock_response = self.client.convert('GBP', 'JPY', 25.00)

        self._assert_structures_match(actual_response, mock_response)

    def test_latest_contract(self):
        actual_response = self.client.latest()

        with patch('api.fixer_api.requests.get') as mock_get:
            output = mock_get.return_value
            output.ok = True
            output.json.return_value = fixer_factory.latest_response(
                GBP=0.72007, JPY=107.346001
            )
            mock_response = self.client.latest()

        for key in ('GBP', 'JPY'):
            self._assert_types_match(actual_response, mock_response, key)

    def test_symbols_contract(self):
        actual_response = self.client.symbols()

        with patch('api.fixer_api.requests.get') as mock_get:
            output = mock_get.return_value
            output.ok = True
            output.json.return_value = fixer_factory.symbols_response(
                EUR='Euro',
                GBP='British Pound Sterling',
                JPY='Japanese Yen',
            )
            mock_response = self.client.symbols()

        for key in ('EUR', 'GBP', 'JPY'):
            self._assert_types_match(actual_response, mock_response, key)
