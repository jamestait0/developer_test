def convert_response(sell_currency, buy_currency, amount, rate):
    return {
        'success': True,
        'query': {
            'from': sell_currency,
            'to': buy_currency,
            'amount': amount,
        },
        'info': {
            'timestamp': 1519328414,
            'rate': rate,
        },
        'historical': '',
        'date': '2018-02-22',
        'result': amount * rate
    }


def latest_response(**currencies):
    return {
        'success': True,
        'timestamp': 1519296206,
        'base': 'EUR',
        'date': '2019-06-18',
        'rates': currencies,
    }


def symbols_response(**currencies):
    return {
        'success': True,
        'symbols': currencies,
    }
