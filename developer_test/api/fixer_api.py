from urllib.parse import (
    urlencode,
    urljoin,
)

import requests

from django.conf import settings


class FixerAPIError(RuntimeError):
    pass


class Client:
    def __init__(self, api_key):
        self.api_key = api_key

    def _get(self, endpoint, **kwargs):
        params = {
            'access_key': self.api_key,
        }
        params.update(kwargs)
        query = urlencode(params)
        path = '{}?{}'.format(endpoint, query)
        url = urljoin(settings.FIXER_API_BASE, path)
        response = requests.get(url)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise FixerAPIError from e
        content = response.json()
        if not content['success']:
            msg = 'Error %(code)d: %(info)s' % content['error']
            raise FixerAPIError(msg)
        return content

    def convert(self, sell_currency, buy_currency, sell_amount):
        response = self._get('convert',
                             sell_currency=sell_currency,
                             buy_currency=buy_currency,
                             amount=sell_amount)
        data = response['query'].copy()
        data['rate'] = response['info']['rate']
        data['result'] = response['result']
        return data

    def latest(self):
        response = self._get('latest')
        return response['rates']

    def symbols(self):
        response = self._get('symbols')
        return response['symbols']
