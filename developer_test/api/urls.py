from django.urls import path

from . import views


urlpatterns = [
    path(r'convert/', views.Convert.as_view()),
    path(r'symbols/', views.SymbolList.as_view()),
    path(r'trades/', views.TradeList.as_view()),
]
