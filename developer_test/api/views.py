from django.conf import settings

from rest_framework import (
    generics,
    status,
)
from rest_framework.response import Response
from rest_framework.views import APIView

from .fixer_api import Client
from forex.models import Trade
from forex.serializers import (
    ConvertSerializer,
    SymbolSerializer,
    TradeSerializer,
)


class TradeList(generics.ListCreateAPIView):
    queryset = Trade.objects.all()
    serializer_class = TradeSerializer


class SymbolList(APIView):
    def get(self, request, format=None):
        fixer = Client(settings.FIXER_API_KEY)
        symbols = fixer.symbols()
        serializer = SymbolSerializer(
            [{
                'symbol': symbol,
                'description': description,
            } for (symbol, description) in symbols.items()],
            many=True)
        return Response(serializer.data)


class Convert(APIView):
    def post(self, request, format=None):
        fixer = Client(settings.FIXER_API_KEY)
        serializer = ConvertSerializer(data=request.data)
        if serializer.is_valid():
            conversion = fixer.convert(**serializer.data)
            serializer = ConvertSerializer({
                'sell_currency': conversion['from'],
                'buy_currency': conversion['to'],
                'sell_amount': conversion['amount'],
                'buy_amount': conversion['result'],
                'rate': conversion['rate'],
            })
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.data)
